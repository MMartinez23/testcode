import numpy as np
import pandas as pd


X = np.random.randint(0,2,size=(1000,8))
y = np.random.randint(0,2,1000)

print(X)
print(y)

X = pd.DataFrame(X, dtype=np.int8)
y = pd.DataFrame(y, dtype=np.int8)

print(X)
print(y)

X.to_csv('X.csv', index=False)
y.to_csv('y.csv', index=False)