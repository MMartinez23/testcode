FROM tensorflow/tensorflow:latest-py3

COPY HelloWorld.py /home/HelloWorld.py
COPY in.py /home/in.py
COPY NewCode.py /home/NewCode.py

ENTRYPOINT ["python"]